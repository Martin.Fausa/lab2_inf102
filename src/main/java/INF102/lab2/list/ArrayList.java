package INF102.lab2.list;

import java.util.Arrays;

public class ArrayList<T> implements List<T> {

	
	public static final int DEFAULT_CAPACITY = 10;
	
	private int n;
	
	private Object elements[];
	
	public ArrayList() {
		elements = new Object[DEFAULT_CAPACITY];
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public T get(int index) {
		// We make sure that the index is within the bounds of the capacity of the elements object.
		// If outside of bounds then exception is thrown, if not then indexed element is returned.
		// Since the method returns T type, we have to cast from Object to T
		if (index < 0 || index >= size()) {
			throw new IndexOutOfBoundsException();
		}
		return (T) elements[index];
	}
	
	@Override
	public void add(int index, T element) { 
		if (index < 0 || index > size()) {
			throw new IndexOutOfBoundsException();
		}
	
		if (size() == elements.length) {
			// We copy the array and double its size
			int newLength = elements.length * 2;
			Object newArray[] = Arrays.copyOf(elements,newLength);
			elements = newArray;
		}
	
		// We shift the elements to make space for the new element
		for (int i = size() - 1; i >= index; i--) {
			elements[i + 1] = elements[i];
		}
	
		elements[index] = element;
		n++;

		

	}
	
	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n*3 + 2);
		str.append("[");
		for (int i = 0; i < n; i++) {
			str.append((T) elements[i]);
			if (i != n-1)
				str.append(", ");
		}
		str.append("]");
		return str.toString();
	}

}